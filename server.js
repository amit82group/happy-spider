var http = require('http'); // 1 - Import Node.js core module

var server = http.createServer(function (req, res) {   // 2 - creating server
    // set response header
    res.writeHead(200, { 'Content-Type': 'text/html' }); 
    // set response content    
    res.write('<html><body><p>Happy Spider will be back up soon</p></body></html>');
    res.end();
});

const PORT = 5000;
server.listen(PORT); //3 - listen for any incoming requests

console.log(`Node.js web server at port ${PORT} is running...`)
